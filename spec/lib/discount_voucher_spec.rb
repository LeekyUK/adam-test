require 'discount_voucher'

describe DiscountVoucher do
  describe '#initialize' do
    it 'sets discount, number and instants from the parameter hash' do
      voucher = DiscountVoucher.new(
        discount: 50,
        number: 3,
        instant: true
      )

      expect(voucher.discount).to eql 50
      expect(voucher.number).to eql 3
      expect(voucher.instant).to be_true
    end
  end

  context 'when charging for each order separately' do
    describe '#billed_for' do
      it 'applies the voucher to the given number of orders' do
        voucher = DiscountVoucher.new(discount: 50, number: 2, instant: false)

        expect(voucher.billed_for).to eql 3.475 # 6.95 / 2
        expect(voucher.billed_for).to eql 3.475 # 6.95 / 2
        expect(voucher.billed_for).to eql 0.00  # Voucher has now expired
      end

      it 'pays for 60% of the order if discount is 40%' do
        voucher = DiscountVoucher.new(discount: 40, number: 1, instant: false)

        expect(voucher.billed_for).to eql 4.17 # 6.95 * 0.6
        expect(voucher.billed_for).to eql 0.00
      end
    end
  end

  context 'when charging up front' do
    describe '#billed_for' do
      it 'bills the entire discount in the first order' do
        voucher = DiscountVoucher.new(discount: 50, number: 3, instant: true)

        expect(voucher.billed_for).to eql -3.475 # User pays ((50% of £6.95) * 3)
        expect(voucher.billed_for).to eql 6.95   # Covered by first payment
        expect(voucher.billed_for).to eql 6.95   # Covered by first payment
        expect(voucher.billed_for).to eql 0.00   # Voucher has now expired
      end

      it 'pays for 60% of the order if discount is 40%' do
        voucher = DiscountVoucher.new(discount: 40, number: 4, instant: true)

        expect(voucher.billed_for).to eql -9.73
      end
    end
  end
end
