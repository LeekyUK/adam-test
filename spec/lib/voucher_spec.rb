require 'voucher'

describe Voucher do
  describe '#create' do
    it 'returns an instance of DefaultVoucher when type is :default' do
      expect(Voucher.create(:default, credit: 42)).
        to be_a(DefaultVoucher)
    end

    it 'returns an instance of DiscountVoucher when type is :discount' do
      expect(Voucher.create(:discount, discount: 50, number: 3)).
        to be_a(DiscountVoucher)
    end

    it 'throws an exception when type is invalid' do
      expect{ Voucher.create(:invalid, {}) }.to raise_error(InvalidVoucherType)
    end
  end
end
