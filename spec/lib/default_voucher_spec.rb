require 'default_voucher'

describe DefaultVoucher do
  describe '#initialize' do
    it 'sets the credit to the parameter hash' do
      voucher = DefaultVoucher.new(credit: 1234)

      expect(voucher.credit).to eql 1234.0
    end
  end

  describe '#billed_for' do
    context 'when there is no credit' do
      it 'is billed for none of the order' do
        voucher = DefaultVoucher.new(credit: 0)

        expect(voucher.billed_for).to eql 0.0
        expect(voucher.credit).to eql 0.0
      end
    end

    context 'when there is enough credit to cover the cost of the order' do
      it 'is billed for the entirety of the order' do
        voucher = DefaultVoucher.new(credit: 10)

        expect(voucher.billed_for).to eql 6.95
        expect(voucher.credit).to eql 3.05
      end
    end

    context 'when there is enough credit to cover part of the order cost' do
      it 'is billed for the remaining credit on the voucher' do
        voucher = DefaultVoucher.new(credit: 2)

        expect(voucher.billed_for).to eql 2.00
        expect(voucher.credit).to eql 0.00
      end
    end
  end
end
