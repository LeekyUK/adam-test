require 'no_voucher'

describe NoVoucher do
  describe '#billed_for' do
    it 'is never billed' do
      voucher = NoVoucher.new

      expect(voucher.billed_for).to eql 0.0
    end
  end
end
