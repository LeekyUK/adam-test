require 'order'

class DiscountVoucher
  attr_reader :discount, :number, :instant

  def initialize(attrs)
    @discount = attrs[:discount]
    @number = attrs[:number]
    @instant = attrs[:instant] || false

    @first_use = true
  end

  def billed_for
    if @instant
      bill_instantly
    else
      bill_weekly
    end
  end

  private

  def use_voucher!
    @first_use = false

    @number -= 1
    @number = 0 if @number < 0
  end

  def bill_weekly
    if @number > 0
      voucher_pays = Order::DEFAULT_PRICE * discount_multiplier
    else
      voucher_pays = 0.0
    end

    use_voucher!
    voucher_pays.round(3)
  end

  def bill_instantly
    if @first_use
      voucher_pays = -((Order::DEFAULT_PRICE * discount_multiplier * @number) - Order::DEFAULT_PRICE)
    elsif @number > 0
      voucher_pays = Order::DEFAULT_PRICE
    else
      voucher_pays = 0.0
    end

    use_voucher!
    voucher_pays.round(3)
  end

  def discount_multiplier
    (100 - @discount) / 100.0
  end
end
