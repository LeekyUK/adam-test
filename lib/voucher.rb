require 'no_voucher'
require 'default_voucher'
require 'discount_voucher'

class Voucher
  attr_accessor :credit, :type

  def self.create(type, attrs)
    case type
    when :default
      DefaultVoucher.new(attrs)
    when :discount
      DiscountVoucher.new(attrs)
    else
      raise InvalidVoucherType
    end
  end
end

class InvalidVoucherType < StandardError; end
