class Order
  DEFAULT_PRICE = 6.95

  attr_accessor :user

  def initialize(user)
    @user = user
  end

  def billed_for
    voucher_pays = 0.0

    # Work with a copy of the voucher, so we
    # don't change its global state
    #
    voucher = @user.voucher.dup

    @user.orders.each do |order|
      voucher_pays = voucher.billed_for
    end

    DEFAULT_PRICE - voucher_pays
  end
end
