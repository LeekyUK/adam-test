require 'order'

class DefaultVoucher
  attr_reader :credit

  def initialize(attrs)
    @credit = Float(attrs[:credit]) || 0.0
  end

  def billed_for
    if @credit >= Order::DEFAULT_PRICE
      voucher_pays = Order::DEFAULT_PRICE
      reduce_credit_by(voucher_pays)
    else
      voucher_pays = @credit
      @credit = 0.0
    end

    voucher_pays.round(3)
  end

  private

  def reduce_credit_by(amount)
    @credit -= amount
    @credit = 0.0 if @credit < 0
  end
end
